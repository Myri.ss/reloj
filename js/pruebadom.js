function modificarJumbotron(){
    console.log("Desde la funcion modificar jumbotron");

    //obtener un elemento del dom
    let jumbotron = document.getElementById("tituloJumbotron");
    console.log(jumbotron);
    console.log(jumbotron.id);
    console.log(jumbotron.className);

    
    jumbotron.className = "text-danger display-4";
    jumbotron.innerText = "Todos queremos Kinder";
}



function transformarTexto(){
    let texto = document.getElementById("inputModificar").value;
    console.log(texto.toUpperCase());
    
    let alerta = document.getElementById("alertMayuscula");
    
    if(texto != ""){
        alerta.className = "alert alert-warning";
        alerta.innerText = texto.toUpperCase(); 
    }else{
        alerta.className = "alert alert-danger";
        alerta.innerText = "AGREGAR EL TEXTO A TRANSFORMAR"; 
    }
}