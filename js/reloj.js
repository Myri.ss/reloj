function obtenerHora() {
    let fecha = new Date();
    // console.log(fecha);
    // console.log(fecha.getDate());
    // console.log(fecha.getMonth());
    // console.log(fecha.getDay());

    let pDiaSemana = document.getElementById("diaSemana"),
        pDia = document.getElementById("dia"),
        pMes = document.getElementById("mes"),
        pAnio = document.getElementById("years"),
        pHoras = document.getElementById("horas"),
        pMinutos = document.getElementById("min"),
        pSegundos = document.getElementById("seg"),
        pAmPm = document.getElementById("ampm");

    let diasReloj = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"];

    let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

    pDiaSemana.innerText = diasReloj[fecha.getDay()];
    pDia.innerText = fecha.getDate();
    pMes.innerText = meses[fecha.getMonth()];
    pAnio.innerText = fecha.getFullYear();

  

    if(fecha.getUTCHours()>= 12){
        pAmPm.innerText = "pm"
        pHoras.innerText = fecha.getHours() -12; //pHoras.innerText = "0" + fecha.getHour() -12;
    
        if(parseInt(pHoras.innerText) == 0){
            pHoras.innerText =12;
        }
    
    }else{
        pHoras.innerText = fecha.getHours();
        pAmPm.innerText = "am";
    }

    if(parseInt(pHoras.innerText) <10){
        pHoras.innerText = "0" + pHoras.innerText
    }

    if (fecha.getMinutes() < 10) {
        pMinutos.innerText = "0" + fecha.getMinutes();
    } else {
        pMinutos.innerText = fecha.getUTCMinutes();
    }

    if (fecha.getSeconds() < 10) {
        pSegundos.innerText = "0" + fecha.getSeconds();
    } else {
        pSegundos.innerText = fecha.getUTCSeconds();
    }
}
window.setInterval(obtenerHora, 1000);
